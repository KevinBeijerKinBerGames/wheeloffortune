﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Changes the tutorial text with a warning if the applied force is not enough.
/// </summary>
public class Warning : MonoBehaviour
{
    private static Text _warningText;

    private void Awake()
    {
        _warningText = GetComponent<Text>();

        ResetText();
    }

    // Resets the text to the tutorial text.
    public static void ResetText()
    {
#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
        _warningText.text = "Swipe by press, hold,\n drag and release.";
#else
        _warningText.text = "Swipe by click, drag\n and release.";
#endif
    }

    // Sets the text to the warning.
    public static void Wrong()
    {
        _warningText.text = "Swipe harder!";
    }

    // Clears the text.
    public static void Right()
    {
        _warningText.text = "";
    }
}
