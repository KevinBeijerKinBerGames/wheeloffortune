﻿using UnityEngine;

/// <summary>
/// Simple Settings Object in which all the wheel prefabs can be stored.
/// </summary>
[CreateAssetMenu]
public class GameSettings : ScriptableObject
{
    public GameObject[] WheelPrefabs;
}
