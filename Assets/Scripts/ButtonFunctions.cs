﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class for the Functions of the UI Buttons.
/// </summary>
public class ButtonFunctions : MonoBehaviour
{
    public GameSettings Settings;

    [SerializeField] private GameObject _quitButton;
    [SerializeField] private GameObject _resetButton;
    [SerializeField] private GameObject _menuScreen;

    private static Swipe _swipe;

    private static GameObject _wheel;
    
    /// <summary>
    /// Turns off the quit button for mobile devices (better to not have a quit button in mobile applications).
    /// </summary>
	private void Awake ()
	{
#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
        _quitButton.SetActive(false);
#endif
	    _swipe = GetComponent<Swipe>();
    }

    private void Update()
    {
        _swipe.enabled = !_menuScreen.activeSelf;

        _resetButton.SetActive(Rotating.Finished);
    }

    /// <summary>
    /// Resets the scores on the wheel by deactivating and reactivating the several parts.
    /// </summary>
    public void ResetButton()
    {
        ScoreCounter.Start = false;

        _swipe.enabled = false;
        Warning.Right();
        _swipe.enabled = true;

        GameObject[] parts = GameObject.FindGameObjectsWithTag("Parts");

        foreach (var part in parts)
        {
            part.SetActive(false);
            part.SetActive(true);
        }
    }

    /// <summary>
    /// Quits the application.
    /// </summary>
    public void QuitButton()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE || UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
        Application.Quit();
#endif
    }

    /// <summary>
    /// Makes the menu pop up.
    /// </summary>
    public void MenuButton()
    {
        Rotating.Finished = true;
        _menuScreen.SetActive(true);

        if (_wheel != null)
        {
            Destroy(_wheel);
        }
    }

    /// <summary>
    /// Picks a random amount of parts in the settings.
    /// Starts the game.
    /// </summary>
    public void RandomButton()
    {
        _wheel = Instantiate(Settings.WheelPrefabs[Mathf.RoundToInt(Random.Range(0, Settings.WheelPrefabs.Length))]);

        _menuScreen.SetActive(false);

        ScoreCounter.Start = false;
        Warning.ResetText();
    }

    /// <summary>
    /// Changes the part amount in the settings.
    /// Starts the game.
    /// </summary>
    public void AmountButton(int amount)
    {
        _wheel = Instantiate(Settings.WheelPrefabs[amount-2]);
            
        _menuScreen.SetActive(false);

        ScoreCounter.Start = false;
        Warning.ResetText();
    }
}
