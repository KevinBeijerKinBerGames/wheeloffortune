﻿using UnityEngine;

/// <summary>
/// Rotates the board.
/// </summary>
public class Rotating : MonoBehaviour
{
    // Minimum and Maximum speed, next to the to be used speed.
    private const float MinSpeed = 3f;
    private const float MaxSpeed = 20f;
    private static float _speed;
    
    // _turning = true if the wheel is turning, _timerStart is used to start a timer to slow the wheel down.
    private static bool _turning;
    private static bool _timerStart;

    // Start time and timer time for slowing the wheel down.
    private float _startTime;
    private float _timer;

    // Finished = true if the wheel has finished turning.
    public static bool Finished;

    private void Awake()
    {
        Finished = true;
        _turning = false;
        _timerStart = false;
    }

    // Fixed Update is used for better collision detection.
	private void FixedUpdate ()
	{
        if (_turning)
        {
            Finished = false;
            if (_timerStart)
            {
                _startTime = Time.time;

                _timerStart = false;
            }
            else
            {
                _timer = Time.time - _startTime;
                var speed = _speed - (_timer*_timer);

                if (speed >= 0f)
                {
                    transform.Rotate(Vector3.forward, speed);
                }
                else
                {
                    Finished = true;
                    _turning = false;
                }
            }
        }
	}
    
    /// <summary>
    /// Checks if the force is strong enough to turn the wheel, and changes the speed accordingly.
    /// </summary>
    /// <param name="force">Force given to it from the Swipe script.</param>
    public static void Turn(float force)
    {
        if (force < MinSpeed)
        {
            Warning.Wrong();
        }
        else
        {
            _turning = true;
            _timerStart = true;

            Warning.Right();

            _speed = force > MaxSpeed ? MaxSpeed : force;
        }
    }
}
