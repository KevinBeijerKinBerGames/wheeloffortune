﻿using UnityEngine;

/// <summary>
/// For spinning the board.
/// </summary>
public class Swipe : MonoBehaviour
{
    private Vector3 _startLocation;
    private Vector3 _endLocation;
    private float _startTime;
    private float _endTime;

    public static float Force;

    private void Update ()
	{
        // You can only spin it when it's not already spinning.
	    if (Rotating.Finished)
	    {
#if UNITY_STANDALONE || UNITY_WEBPLAYER
            // For if you're playing on PC it uses the left mouse button.
	        if (Input.GetMouseButtonDown(0))
	        {
	            _startLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	            _startTime = Time.time;
	        }

	        if (Input.GetMouseButtonUp(0))
	        {
	            _endLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	            _endTime = Time.time;

	            CalculateForce();
	        }
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
            // When you're playing on a mobile device it will use touch and drag mechanics.
            if (Input.touchCount > 0)
            {
                var myTouch = Input.touches[0];

                if (myTouch.phase == TouchPhase.Began)
                {
                    _startLocation = myTouch.position;
                    _startTime = Time.time;
                }
                else if (myTouch.phase == TouchPhase.Ended)
                {
                    _endLocation = myTouch.position;
                    _endTime = Time.time;

                    CalculateForce();
                }
            }
#endif
        }
    }

    /// <summary>
    /// Calulates the force with which the wheel gets spun.
    /// </summary>
    private void CalculateForce()
    {
        var directionX = _endLocation.x - _startLocation.x;
        var directionY = _endLocation.y - _startLocation.y;
        var movementTime = _endTime - _startTime;

        Force = Mathf.Sqrt(directionX * directionX + directionY * directionY) / movementTime;

        Rotating.Turn(Force);
    }
}
