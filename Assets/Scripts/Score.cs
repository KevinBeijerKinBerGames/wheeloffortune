﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Determines the worth of each separate part.
/// </summary>
public class Score : MonoBehaviour
{
    // A bigger range doesn't fit the parts.
    [Range(-10000, 10000)]
    public int Worth;

    // If _randomWorth is true, make the worth random, otherwise it will use the set Worth variable.
    [SerializeField] private bool _randomWorth = true;

    private Text _scoreText;

    // Sets a "cheap" random worth + changes the text accordingly.
    private void OnEnable()
    {
        _scoreText = GetComponent<Text>();

        if (_randomWorth)
        {
            Worth = Mathf.RoundToInt(Random.Range(-10f, 10f))*1000;
        }
        
        _scoreText.text = "" + Worth;
    }

}
