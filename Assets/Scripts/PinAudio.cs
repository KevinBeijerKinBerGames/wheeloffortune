﻿using UnityEngine;

/// <summary>
/// Plays an audio clip every time a peg on the board gets hit.
/// </summary>
public class PinAudio : MonoBehaviour
{
    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Pegs")) return;
        _audioSource.Play();
    }
}
