﻿using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

/// <summary>
/// Reads the score from the parts and changes the score texts appropriately.
/// </summary>
public class ScoreCounter : MonoBehaviour
{
    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _totalScoreText;

    private int _score;
    private int _totalScore;

    public static bool Start = false;
    private bool _setScore = false;

    /// <summary>
    /// Sets the score text and changes the total score when the rotation is finished.
    /// </summary>
    private void Update()
    {
        if (!Start)
        {
            _score = 0;
        }

        if (!Rotating.Finished)
        {
            Start = true;
            _setScore = true;
        }
        else
        {
            if (_setScore)
            {
                _totalScore += _score;
                _totalScoreText.text = "Total score: " + _totalScore;

                _setScore = false;
            }
            _scoreText.text = "Your score = " + _score;
        }       
    }

    /// <summary>
    /// Gets score from parts.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Parts"))
        {
            _score = other.GetComponent<Score>().Worth;
        }
    }
}
